﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameQuitter : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        //quit game using escape
        if (Input.GetButtonDown("quit"))
        {
            
            Application.Quit();
        }

    }
}
