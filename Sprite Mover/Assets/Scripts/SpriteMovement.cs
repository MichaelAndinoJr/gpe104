﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMovement : MonoBehaviour
{
    // Create a variable for our transform component
    private Transform tf; // variable for transform component
    public float Speed; // variable to adjust speed of character 
                        // public float maxMovement; // Create a variable for the max we can move in one frame draw
                        // Start is called before the first frame update
    void Start()
    {
        // Load our transform component into our variable
        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {

        //Movement for x and y axis 



        //Movement during shift Working!!!
        if (Input.GetKey(KeyCode.LeftShift)) //&& Input.GetKeyDown("right"))
        {

            if (Input.GetKeyDown("right"))
            {
                float X = Input.GetAxis("movementX");
                tf.position += Vector3.right;

            }
            if (Input.GetKeyDown("left"))
            {
                float X = Input.GetAxis("movementX");
                tf.position += Vector3.left;
            }
            if (Input.GetKeyDown("up"))
            {
                float Y = Input.GetAxis("movementY");
                tf.position += Vector3.up;
            }
            if (Input.GetKeyDown("down"))
            {
                float Y = Input.GetAxis("movementY");
                tf.position += Vector3.down;
            }
        }
        else
        {
            float axesValueX = Input.GetAxis("movementX");
            float axesValueY = Input.GetAxis("movementY");
            float TotalSpeed = axesValueX * Speed;
            tf.position += Vector3.right * TotalSpeed;
            float TotalSpeedY = axesValueY * Speed;
            tf.position += Vector3.up * TotalSpeedY;
        }
        //Send our character back to origin when space is pressed
        if (Input.GetButtonDown("reset"))
        {
            tf.position = Vector3.zero;
        }

       
    }
}