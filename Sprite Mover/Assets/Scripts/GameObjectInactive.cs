﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectInactive : MonoBehaviour
{
    //variable for game object
    public GameObject myChar;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("inactive"))
        {
            //disables game object
            myChar.SetActive(false);
        }

    }



}