﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementComponentToggle : MonoBehaviour
{
    //variable for component called
    public SpriteMovement MovementDisabler;

    // Start is called before the first frame update
    void Start()
    {
        MovementDisabler = GetComponent<SpriteMovement>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            //disables movement component, the MovementDisabler.enabled makes this a toggle. meaning it will set MovementDisabler to what it is not at the moment i.e true or false
            MovementDisabler.enabled = !MovementDisabler.enabled;

        }

    }
}
